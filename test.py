#!/usr/bin/env python3

import subprocess
import sys
import os
import shutil
from typing import List

def qml2cpp(files: List[str]):
    subprocess.call(["qml2cpp"] + files)

def moc(files: List[str]):
    for file_p in files:
        moc_output = subprocess.check_output(["moc", file_p]).decode()
        f = open(file_p, "a")
        f.write(moc_output)
        f.close()

def main_stub(file_p: str):
    f = open(file_p, "w")
    f.write(
"""
#include <iostream>
#include <QDebug>

#include <___main_qml.cpp>

#include <QGuiApplication>

int main(int argc, char** argv) {
    QGuiApplication app(argc, argv);
    Main m;
    std::cout << m.quadN() << std::endl << m.nPlusOne();
    qDebug() << m.r() << m.s();
    return app.exec();
}
""")
    f.close()

def qmake_query(key: str) -> str:
    return subprocess.check_output(["qmake", "-query", key]).decode().strip()

def comp(file_p: str):
    header_dir = qmake_query("QT_INSTALL_HEADERS")
    lib_dir    = qmake_query("QT_INSTALL_LIBS")
    cmd = ["g++", "-O2", file_p, "-I.", f"-I{header_dir}",  f"-I{header_dir}/QtCore", f"-I/{header_dir}/QtQuick", f"-I{header_dir}/QtGui", f"-I{header_dir}/QtQml", "-fPIC", "-std=c++17", f"-L{lib_dir}", "-lQt6Core", "-lQt6Quick", "-lQt6Qml", "-lQt6Gui", "-flto", "-fno-exceptions", "-o", "test"]
    print(" ".join(cmd))
    subprocess.call(cmd)

def mangle_filename(name: str) -> str:
    return name.replace("/", "_").replace(".", "_") + ".cpp"

if os.path.isdir("testbuild"):
    shutil.rmtree("testbuild")
os.mkdir("testbuild")
os.chdir("testbuild")

files = ["../Add.qml", "../Sub.qml", "../main.qml"]
qml2cpp(files)
moc([mangle_filename(f) for f in files])
main_stub("test.cpp")
comp("test.cpp")
