#include <span>
#include <string_view>
#include <memory>
#include <iostream>

#include "typesystem.h"
#include "pipeline.h"

using namespace std;

int main(int argc, char *argv[])
{
    auto args = std::span(argv, argc);

    for_each(args.begin() + 1, args.end(), [&](const string &file) {
        compile_file(Typesystem::instance(), file);
    });
}
