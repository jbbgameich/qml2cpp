#pragma once

#include <nlohmann/json.hpp>

#include <unordered_map>
#include <string>
#include <vector>
#include <tuple>
#include <optional>

#include "typesystem.h"
#include "jsast.h"

using namespace std;

namespace IR {
struct QmlType;
struct QmlParameter;

/// property declaration
struct QmlProperty {
    QmlTypeId type;
    string name;
    JsExpression binding;
    bool readonly;
};

/// property binding
struct QmlBinding {
    string name;
    JsExpression binding; // TODO
};

struct QmlMethod {
    enum Type {
        Signal = 0,
        Function = 1
    };

    Type type;
    string name;
    vector<QmlParameter> parameters;
};

struct QmlParameter {
    string name;
    string type_name;
};

struct QmlType {
    string name;
    string id;
    optional<string> import;
    string superclass;
    string file_path;
    unordered_map<string, QmlProperty> properties;
    vector<QmlBinding> bindings;
    unordered_map<string, QmlMethod> methods;
    vector<QmlType> children;
};

struct QmlImport {
    optional<string> importId;
    string uri;
};

class IntermediateRepresentation
{
public:
    IntermediateRepresentation(const string &path, const nlohmann::json &dom);
    QmlType parse_qml_type(const nlohmann::json &object);
    QmlType parse_qml_component(const nlohmann::json &component);
    QmlTypeId split_namespace_and_type(std::string_view name) const;

    unordered_map<string, QmlImport> imports;
    unordered_map<string, QmlType> qml_types;
    string qml_file;
    vector<string> includes;
};
};
