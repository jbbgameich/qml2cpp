#include "utils.h"

#include <iostream>

void replace(string &in, const string &search, const string &replacement) {
    for (std::string::size_type pos = in.find(search); pos != std::string::npos; pos = in.find(search, pos + 1)) {
        in.replace(pos, search.size(), replacement);
    }
}

void indent(ostream &stream, int depth)
{
    for (int i = 0; i <= depth; i++) {
        stream << "  ";
    }
}
