#include <string_view>
#include <span>
#include <vector>

#include <cstdio>
#include <memory>
#include <string>
#include <array>


#include "qmldom.h"

using namespace std;

std::string execute_command(const std::vector<std::string_view> &command) {
    std::string cmd;
    for (const auto &item : command) {
        cmd.append(" ");
        cmd.append(item);
    }

    std::array<char, 128> buffer = {};
    std::string result;

    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.data(), "r"), pclose);
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }

    return result;
}

nlohmann::json qmldom(string_view qml_entrypoint) {
    auto output = execute_command({"qmldom", qml_entrypoint});
    return nlohmann::json::parse(output);
}
