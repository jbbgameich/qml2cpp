﻿#pragma once

#include <fstream>
#include <string>
#include <vector>

using namespace std;

namespace IR {
class IntermediateRepresentation;
struct QmlProperty;
struct QmlType;
}

class Typesystem;

namespace CG {
class Names {
public:
    static string getter_name(const string &property_name);
    static string setter_name(const string &property_name);
    static string signal_name(const string &property_name);
    static string value_name(const string &property_name);
    static string to_uppercase(string name);
    static string class_name(const string &name);
    static string bindable_name(const string &name);
    static string child_class_name(const string &parent_name, string import, const string &superclass, int i);
    static string proxy_value_name(const string &property_name);
    static string proxy_signal_name(const string &property_name);
    static string proxy_update_function_name(const string &property_name);
};

class Codegen
{
public:
    Codegen(const IR::IntermediateRepresentation &ir, Typesystem &types);

    void generate_code();

private:
    void generate_class(const IR::QmlType &type);
    void generate_imports(const vector<IR::QmlType> &types);

    const IR::IntermediateRepresentation &m_ir;
    const Typesystem &m_types;
    std::ofstream m_out;
};
}
