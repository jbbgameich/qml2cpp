#pragma once

#include <string>

using namespace std;

void replace(string &in, const string &search, const string &replacement);
void indent(ostream &stream, int depth);
