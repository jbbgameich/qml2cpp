#pragma once

#include <string_view>
#include <vector>
#include <string>
#include <memory>
#include <cassert>
#include <iostream>
#include <optional>

#include <pugixml.hpp>

#include "utils.h"
#include "typesystem.h"

using namespace std;

#define NOT_IMPLEMENTED \
    std::cerr << "ERROR: A not implemented functionality was reached" << endl; \
    std::flush(std::cerr); \
    assert(false);

#define ASSERT_EXPLAIN(expr, explanation) \
    if (!(expr)) { \
        std::cerr << explanation << endl; \
    } \
    assert(expr); \

#define UNREACHABLE \
    std::cerr << "ERROR: Unreachable code was reached" << endl; \
    std::flush(std::cerr); \
    assert(false);

void fatal_type_error(const QmlTypeId &type_a, const QmlTypeId &type_b);

struct AstNode {
    virtual ~AstNode() = default;

    [[nodiscard]] virtual string_view node_name() const = 0;

    vector<shared_ptr<AstNode>> children;
};

struct TypedAstNode : public AstNode {
    /// This function must only be used after building the ast.
    /// It will fail if infer_and_store_type has not been called previously on the node
    QmlTypeId get_inferred_type() const;

    /// This function should be used while building the ast.
    /// It figures out the type of the node and of its children, and stores it for the codegen phase.
    QmlTypeId infer_and_store_type(const optional<QmlTypeId> &hint);

protected:
    optional<QmlTypeId> inferred_type = nullopt;

    [[nodiscard]] virtual QmlTypeId infer_type(const optional<QmlTypeId> &hint) const = 0;
};

struct LiteralAstNode : public TypedAstNode {
    [[nodiscard]] virtual string_view display_value() const = 0;
};

struct ExpressionStatement : public TypedAstNode {
    ExpressionStatement() = default;
    ExpressionStatement(const QmlTypeId &type) {
        inferred_type = type;
    }
    virtual QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override;

    string_view node_name() const override {
        return "ExpressionStatement";
    }
};

struct StringLiteral : public LiteralAstNode {
    string value;

    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override;

    string_view node_name() const override {
        return "StringLiteral";
    }

    string_view display_value() const override {
        return value;
    }
};

struct NumericLiteral : public LiteralAstNode {
    string value; // Sorry

    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override;

    string_view node_name() const override {
        return "NumericLiteral";
    }

    string_view display_value() const override {
        return value;
    }
};

struct BinaryExpression : public TypedAstNode {
    enum Op {
        Add,
        Sub,
        Mul,
        Div,
        Eq,
        Neq,
        And,
        Or
    };

    Op op;
    string operatorToken;

    [[nodiscard]] shared_ptr<AstNode> operand_a() const {
        return children.at(0);
    }

    [[nodiscard]] shared_ptr<AstNode> operand_b() const {
        return children.at(1);
    }

    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override;

    string_view node_name() const override {
        return "BinaryExpression";
    }
};

struct IdentifierExpression : public TypedAstNode {
    string name;

    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override;

    string_view node_name() const override {
        return "IdentifierExpression";
    }
};

struct ConditionalExpression : public TypedAstNode {
    [[nodiscard]] shared_ptr<AstNode> condition() const {
        return children.at(0);
    }

    [[nodiscard]] shared_ptr<AstNode> branch_if() const {
        return children.at(1);
    }
    [[nodiscard]] shared_ptr<AstNode> branch_else() const {
        return children.at(2);
    }

    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override;

    string_view node_name() const override {
        return "ConditionalExpression";
    }
};

struct TrueLiteral : public LiteralAstNode {
    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override {
        return QmlTypeId("bool");
    }

    string_view node_name() const override {
        return "TrueLiteral";
    }

    string_view display_value() const override {
        return "true";
    }
};

struct FalseLiteral : public LiteralAstNode {
    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override {
        return QmlTypeId("bool");
    };

    string_view node_name() const override {
        return "FalseLiteral";
    }

    string_view display_value() const override {
        return "false";
    }
};

struct NotExpression : public TypedAstNode {
    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override {
        ASSERT_EXPLAIN((children.size() == 1), "A not expression can invert exactly one following expression");

        if (hint && !Typesystem::instance().can_construct_from(QmlTypeId("bool"), operand()->infer_and_store_type(QmlTypeId("bool")))) {
            fatal_type_error(*hint, QmlTypeId("bool"));
        }

        return QmlTypeId("bool");
    }

    string_view node_name() const override {
        return "NotExpression";
    }

    shared_ptr<TypedAstNode> operand() const {
        ASSERT_EXPLAIN((children.size() == 1), "A not expression can invert exactly one following expression");
        auto opr = dynamic_pointer_cast<TypedAstNode>(children.front());
        ASSERT_EXPLAIN(opr, "The operand to a not expression needs be an expression that has a type");
        return opr;
    }
};

struct NestedExpression : public TypedAstNode {
    QmlTypeId infer_type(const optional<QmlTypeId> &hint) const override;

    shared_ptr<TypedAstNode> content() const;

    string_view node_name() const override {
        return "NestedExpression";
    }
};

class JsExpression
{
public:
    JsExpression(string xmlast, optional<QmlTypeId> return_type);
    JsExpression() {
        ast = make_shared<ExpressionStatement>();
    };
    JsExpression(optional<QmlTypeId> return_type) {
        if (return_type) {
            ast = make_shared<ExpressionStatement>(*return_type);
        } else {
            ast = make_shared<ExpressionStatement>();
        }
    }
    JsExpression(JsExpression &&other) {
        ast = other.ast;
    }
    JsExpression(const JsExpression &other) {
        ast = other.ast;
    };

    JsExpression &operator=(JsExpression &&other) {
        ast = other.ast;
        return *this;
    }

    shared_ptr<TypedAstNode> ast;

private:
    void load_ast(pugi::xml_node xml_parent, shared_ptr<AstNode> ast_parent, int depth = 0);
};
