#include "qmldom.h"
#include "typesystem.h"
#include "codegen.h"
#include "ir.h"

void compile_file(Typesystem &ts, const string &path)
{
    auto json = qmldom(path);
    IR::IntermediateRepresentation ir(path, json);
    CG::Codegen cg(ir, ts);
    cg.generate_code();
}
