#include "typesystem.h"

#include <iostream>

#include <ranges>

namespace ranges = std::ranges;

Typesystem::Typesystem()
{
    m_qml_types = {
        { QmlTypeId { "QtQuick", "Item" }, CppTypeId { "QQuickItem", "QQuickItem" }},
        { QmlTypeId { "date" }, CppTypeId { "QDateTime", "JsDate" }},
        { QmlTypeId { "point" }, CppTypeId { "QPoint", "JsPoint" }},
        { QmlTypeId { "rect" }, CppTypeId { "QRect", "JsRect" }},
        { QmlTypeId { "size" }, CppTypeId { "QSize", "JsSize" }},
        { QmlTypeId { "QtQuick.Window", "Window"}, CppTypeId { "QQuickWindow", "QQuickWindow" }},
        { QmlTypeId { "QtQml", "QtObject" }, CppTypeId { "QObject", "QObject" }},
        // builtin types
        { QmlTypeId { "string" }, CppTypeId { "QString", "QString" }},
        { QmlTypeId { "int" }, CppTypeId {"cstdint", "int32_t" }},
        { QmlTypeId { "double" }, CppTypeId { "stdint", "double" }},
        { QmlTypeId { "real" }, CppTypeId { "QtGlobal", "qreal" }},
        { QmlTypeId { "bool" }, CppTypeId { "stdint", "bool" }},
        { QmlTypeId { "url" }, CppTypeId { "QString", "QString" }}, // TODO check if this is correct
        { QmlTypeId { "var" }, CppTypeId { "QVariant", "QVariant" }},
        { QmlTypeId { "void" }, CppTypeId { "stdint", "void" }}
    };

    m_conversions = {
        { QmlTypeId { "date" }, { QmlTypeId { "string" } } },
        { QmlTypeId { "point" }, { QmlTypeId { "string" } } },
        { QmlTypeId { "rect" }, { QmlTypeId { "string" } } },
        { QmlTypeId { "size" }, { QmlTypeId { "string" } } },
        { QmlTypeId { "date" }, { QmlTypeId { "string" } } },
        { QmlTypeId { "int" }, { QmlTypeId { "double"}, QmlTypeId { "real" } } },
        { QmlTypeId { "url" }, { QmlTypeId { "string"} } },
        { QmlTypeId { "double" }, { QmlTypeId { "real"} } },
        { QmlTypeId { "real" }, { QmlTypeId { "double"} } }
    };

    m_comparable = {
        { QmlTypeId { "point" }, { QmlTypeId { "point" } } },
        { QmlTypeId { "rect" }, { QmlTypeId { "rect" } } },
        { QmlTypeId { "size" }, { QmlTypeId { "size" } } },
        { QmlTypeId { "string" }, { QmlTypeId { "string" } } },
        { QmlTypeId { "int" }, { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" }, QmlTypeId { "bool" } } },
        { QmlTypeId { "double" }, { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" }, QmlTypeId { "bool" } } },
        { QmlTypeId { "real" }, { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" }, QmlTypeId { "bool" } } },
        { QmlTypeId { "bool" }, { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" }, QmlTypeId { "bool" } } },
        { QmlTypeId { "url" }, { QmlTypeId { "string" }, QmlTypeId {"url"} } },
        { QmlTypeId { "var" }, { QmlTypeId { "var" }, QmlTypeId { "int" } } }
    };

    m_addable = {
        { QmlTypeId { "int" },  { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } },
        { QmlTypeId { "double" }, { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } },
        { QmlTypeId { "real" },  { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } },
    };

    m_substractable = {
        { QmlTypeId { "int" },  { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } },
        { QmlTypeId { "double" }, { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } },
        { QmlTypeId { "real" },  { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } }
    };

    m_dividable = {
        { QmlTypeId { "int" },  { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } },
        { QmlTypeId { "double" }, { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } },
        { QmlTypeId { "real" },  { QmlTypeId { "int" }, QmlTypeId { "double" }, QmlTypeId { "real" } } }
    };

    m_multiplyable = m_dividable;
}

Typesystem &Typesystem::instance()
{
    static Typesystem typesystem;
    return typesystem;
}

CppTypeId Typesystem::get_cpptype_of_qmltype(const optional<string> &import, const string &qmlname) const {
    return get_cpptype_of_qmltype(QmlTypeId { import, qmlname });
}

CppTypeId Typesystem::get_cpptype_of_qmltype(const QmlTypeId &type) const
{
    std::cout << " Type lookup for " << (type.import ? *type.import : "builtin") << " " << type.name << std::endl;
    std::flush(std::cout);

    QmlTypeId qmltype { type.import, type.name };

    if (!m_qml_types.contains(qmltype)) {
        cerr << "Error: The type " << type.name << " is not defined. If it is a type defined in your project, add its file to the qml2cpp invokation before the first file that uses it.";
        exit(1);
    }

    return m_qml_types.at(qmltype);
}

void Typesystem::register_type(const string &import, const string &qmlname, const CppTypeId &cpptype)
{
    m_qml_types[QmlTypeId { import, qmlname }] = cpptype;
}

void Typesystem::register_builtin_type(const string &qmlname, const CppTypeId &cpptype)
{
    m_qml_types[QmlTypeId { {}, qmlname }] = cpptype;
}

bool Typesystem::can_construct_from(const QmlTypeId &type, const QmlTypeId &from) const
{
    if (type == from) {
        return true;
    }

    if (!m_conversions.contains(type))
        return false;

    auto it = ranges::find(m_conversions.at(type), from);
    return it != m_conversions.at(type).end();
}

bool Typesystem::can_compare_to(const QmlTypeId &type, const QmlTypeId &to) const
{
    if (!m_comparable.contains(type)) {
        return false;
    }

    const auto &comparisons = m_comparable.at(type);
    return ranges::find(comparisons, to) != comparisons.cend();
}

bool Typesystem::can_add_to(const QmlTypeId &type, const QmlTypeId &to) const
{
    if (!m_addable.contains(type)) {
        return false;
    }

    const auto &additions = m_addable.at(type);
    return ranges::find(additions, to) != additions.cend();
}

bool Typesystem::can_substract_from(const QmlTypeId &type, const QmlTypeId &from) const
{
    if (!m_substractable.contains(type)) {
        return false;
    }

    const auto &substractions = m_substractable.at(type);
    return ranges::find(substractions, from) != substractions.cend();
}

bool Typesystem::can_divide(const QmlTypeId &type, const QmlTypeId &by) const
{
    if (!m_dividable.contains(type)) {
        return false;
    }

    const auto &divisons = m_dividable.at(type);
    return ranges::find(divisons, by) != divisons.cend();
}

bool Typesystem::can_multiply(const QmlTypeId &type, const QmlTypeId &to) const
{
    if (!m_multiplyable.contains(type)) {
        return false;
    }

    const auto &multiplications = m_multiplyable.at(type);
    return ranges::find(multiplications, to) != multiplications.cend();
}

QmlTypeId Typesystem::larger_num_type(const QmlTypeId &a, const QmlTypeId &b) const
{
    static vector<QmlTypeId> num_type_precedence({QmlTypeId("int"), QmlTypeId("real"), QmlTypeId("double")});

    if (distance(num_type_precedence.begin(), ranges::find(num_type_precedence, a))
            > distance(num_type_precedence.begin(), ranges::find(num_type_precedence, b))) {
        return a;
    } else {
        return b;
    }
}
