#pragma once

#include <string_view>

class Typesystem;

using namespace std;

///
/// \brief Compile a single qml file
/// \param Typesystem used to look up files from the current project
/// \param Path to the qml file to compile
///
void compile_file(Typesystem &ts, const string &path);
