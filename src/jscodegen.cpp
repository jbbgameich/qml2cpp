#include "jscodegen.h"

#include <fmt/format.h>

JsCodegen::JsCodegen(const JsExpression &ast)
    : expr(ast)
{
}

string JsCodegen::generate_cpp()
{
    generate(expr.ast);
    return out.str();
}

class ExpressionGuard {
public:
    ExpressionGuard(ostream &out, const optional<QmlTypeId> &type = {})
        : out(out)
    {
        out << "[&]() ";
        if (type) {
            out << "-> " << Typesystem::instance().get_cpptype_of_qmltype(*type).name;
        }
        out << " { ";
    }

    ~ExpressionGuard() {
        out << "; }()";
    }

private:
    ostream &out;
};

void JsCodegen::generate(shared_ptr<AstNode> ast)
{
    if (const auto node = dynamic_pointer_cast<ExpressionStatement>(ast)) {
        ExpressionGuard g(out, node->get_inferred_type());
        if (!node->children.empty()) {
            std::for_each(node->children.begin(), node->children.end() - 1, [&](const auto &child) {
                generate(child);
            });

            out << "return ";
            generate(node->children.back());
        } else {
            const auto qmltype = node->get_inferred_type();
            out << "return " << Typesystem::instance().get_cpptype_of_qmltype(qmltype.import, qmltype.name).name << "{}"; // Default construct something to return
        }
    } else if (const auto node = dynamic_pointer_cast<StringLiteral>(ast)) {
        out << R"(QStringLiteral(")" << node->value << R"("))";
    } else if (const auto node = dynamic_pointer_cast<NumericLiteral>(ast)) {
        out << node->value;
    } else if (const auto node = dynamic_pointer_cast<ConditionalExpression>(ast)) {
        ExpressionGuard g(out);
        out << "if (";
        generate(node->condition());
        out << ") { return ";
        generate(node->branch_if());
        out << "; } else { return ";
        generate(node->branch_else());
        out << "; }";
    } else if (const auto node = dynamic_pointer_cast<BinaryExpression>(ast)) {
        out << "(";
        generate(node->operand_a());
        out << " ";
        switch(node->op) {
        case BinaryExpression::Add:
            out << "+";
            break;
        case BinaryExpression::Sub:
            out << "-";
            break;
        case BinaryExpression::Mul:
            out << "*";
            break;
        case BinaryExpression::Div:
            out << "/";
            break;
        case BinaryExpression::Eq:
            out << "==";
            break;
        case BinaryExpression::Neq:
            out << "!=";
            break;
        case BinaryExpression::And:
            out << "&&";
            break;
        case BinaryExpression::Or:
            out << "||";
            break;
        }
        out << " ";
        generate(node->operand_b());
        out << ")";
    } else if (const auto node = dynamic_pointer_cast<IdentifierExpression>(ast)) {
        const auto type = node->get_inferred_type();
        out << Typesystem::instance().get_cpptype_of_qmltype(type.import, type.name).name;
        out << "(m_" << node->name << ")";
        // TODO handle id resolving
    } else if (const auto node = dynamic_pointer_cast<TrueLiteral>(ast)) {
        out << "true";
    } else if (const auto node = dynamic_pointer_cast<FalseLiteral>(ast)) {
        out << "false";
    } else if (const auto node = dynamic_pointer_cast<NotExpression>(ast)) {
        out << "!(";
        generate(node->operand());
        out << ")";
    } else if (const auto node = dynamic_pointer_cast<NestedExpression>(ast)) {
        out << "(";
        generate(node->content());
        out << ")";
    } else {
        cerr << "Can't generate code for " << ast->node_name() << endl;
        UNREACHABLE
    }
}


