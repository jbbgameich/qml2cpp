#include "codegen.h"

#include <iostream>
#include <fstream>
#include <cassert>
#include <ranges>

#include <fmt/format.h>

#include "ir.h"
#include "typesystem.h"
#include "utils.h"
#include "jscodegen.h"

namespace CG {
string Names::getter_name(const string &property_name)
{
    return property_name;
}

string Names::setter_name(const string &property_name)
{
    return fmt::format("set{}", to_uppercase(property_name));
}

string Names::signal_name(const string &property_name)
{
    return fmt::format("{}Changed", property_name);
}

string Names::value_name(const string &property_name)
{
    return fmt::format("m_{}", property_name);
}

string Names::to_uppercase(string name)
{
    name[0] = char(toupper(name[0]));
    return name;
}

string Names::class_name(const string &name)
{
    return to_uppercase(name);
}

string Names::bindable_name(const string &name)
{
    return fmt::format("bindable{}", to_uppercase(name));
}

string Names::child_class_name(const string &parent_name, string import, const string &superclass, int i)
{
    string::size_type position = 0;
    while ((position = import.find('.')) != std::string::npos) {
        import.replace(position, 1, "");
    }
    return fmt::format("{}Child{}{}{}", Names::class_name(parent_name), import, superclass, i);
}

string Names::proxy_value_name(const string &property_name)
{
    return Names::value_name(fmt::format("proxy{}", to_uppercase(property_name)));
}

string Names::proxy_signal_name(const string &property_name)
{
    return Names::signal_name(fmt::format("proxy{}", to_uppercase(property_name)));
}

string Names::proxy_update_function_name(const string &property_name)
{
    return Names::signal_name(fmt::format("updateProxy{}", to_uppercase(property_name)));
}

Codegen::Codegen(const IR::IntermediateRepresentation &ir, Typesystem &types)
    : m_ir(ir)
    , m_types(types)
{
    auto out_name = ir.qml_file;
    replace(out_name, "/", "_");
    replace(out_name, ".", "_");

    out_name += ".cpp";

    cout << "Generating " << out_name << endl;

    m_out = ofstream(out_name, ios::binary);

    for (const auto &[_, type] : m_ir.qml_types) {
        auto cpp_class_name = Names::class_name(type.name);
        types.register_builtin_type(type.name, CppTypeId { out_name, cpp_class_name });
    }
}

void Codegen::generate_code()
{
    m_out <<
R"(
#ifndef QML2CPP_PRELUDE
#define QML2CPP_PRELUDE

#include <QDateTime>
#include <QString>

class JsDate : public QDateTime {
public:
    JsDate() : QDateTime() {}

    inline JsDate(const QString &dateString) {
        const auto qdatetime = QDateTime::fromString(dateString, Qt::ISODate);
        *this = *static_cast<const JsDate *>(&qdatetime);
    }
};

#include <QPoint>
#include <string_view>
#include <QStringView>
#include <QQmlProperty>

inline qreal toFloat(std::u16string_view string) {
    bool ok;
    auto f = QStringView(string).toFloat(&ok);
    Q_ASSERT(ok);
    return f;
}

static_assert(sizeof(char16_t) == sizeof(QChar));

class JsPoint : public QPointF {
public:
    JsPoint() : QPointF() {}

    inline JsPoint(std::u16string_view pointStr) {
        auto pos = pointStr.find(',');
        Q_ASSERT(pos != std::string::npos);

        auto x = toFloat(pointStr.substr(0, pos));
        auto y = toFloat(pointStr.substr(pos + 1, pointStr.size() - (pos + 1)));

        setX(x);
        setY(y);
    }

    inline JsPoint(const QString &pointStr) : JsPoint(std::u16string_view(reinterpret_cast<const char16_t *>(pointStr.data()), pointStr.size())) {}
};

#include <QRect>

class JsRect : public QRectF {
public:
    JsRect() : QRectF() {}

    inline JsRect(std::u16string_view rectStr) {
        auto firstCommaPos = rectStr.find(',');
        Q_ASSERT(firstCommaPos != std::string::npos);
        auto x = toFloat(rectStr.substr(0, firstCommaPos)); // atof generates a lot better code than std::stof and avoids std::string allocation

        auto secondCommaPos = rectStr.find(',', firstCommaPos + 1);
        Q_ASSERT(secondCommaPos != std::string::npos);
        auto y = toFloat(rectStr.substr(firstCommaPos + 1, secondCommaPos - (firstCommaPos + 1)));

        auto xPos = rectStr.find('x', secondCommaPos + 1);
        Q_ASSERT(xPos != std::string::npos);
        auto w = toFloat(rectStr.substr(secondCommaPos + 1, xPos - (secondCommaPos + 1)));

        auto h = toFloat(rectStr.substr(xPos + 1, rectStr.size() - (xPos + 1)));

        setX(x);
        setY(y);
        setWidth(w);
        setHeight(h);
    }

    inline JsRect(const QString &rectStr) : JsRect(std::u16string_view(reinterpret_cast<const char16_t *>(rectStr.data()), rectStr.size())) {}
};

#include <QSize>

class JsSize : public QSizeF {
public:
    JsSize() : QSizeF() {}

    inline JsSize(std::u16string_view sizeStr) {
        auto xPos = sizeStr.find('x');
        Q_ASSERT(xPos != std::string::npos);
        auto w = toFloat(sizeStr.substr(0, xPos));

        auto h = toFloat(sizeStr.substr(xPos + 1, sizeStr.size() - (xPos + 1)));

        setWidth(w);
        setHeight(h);
    }

    inline JsSize(const QString &sizeStr) : JsSize(std::u16string_view(reinterpret_cast<const char16_t *>(sizeStr.data()), sizeStr.size())) {}
};

#include <QQuickWindow>

void parentWindow(QObject *self, QQuickWindow *childWindow) {
    QQuickWindow *parentWindow = nullptr;
    QObject *currentObj = self;
    while (!parentWindow && currentObj) {
        if (auto window = qobject_cast<QQuickWindow *>(currentObj)) {
            parentWindow = window;
        } else {
            currentObj = currentObj->parent();
        }
    }
    Q_ASSERT(parentWindow);
    childWindow->setParent(parentWindow);
}

#endif
)";

    vector<IR::QmlType> types;
    ranges::transform(m_ir.qml_types, back_inserter(types), [](const auto kv){
        return kv.second;
    });
    generate_imports(types);

    for (const auto &type : types) {
        m_out << fmt::format(R"(
#ifndef {class_name}_CODE
#define {class_name}_CODE
)", fmt::arg("class_name", Names::class_name(type.name)));

        generate_class(type);

        m_out << "#endif" << endl;
    }
}

void Codegen::generate_class(const IR::QmlType &type)
{
    int child_i = 0;
    for (const auto &child : type.children) {
        auto child_type = child;
        child_type.name = Names::child_class_name(type.name, child_type.import ? *child_type.import : "", child_type.superclass, child_i);
        generate_class(child_type);
        child_i++;
    }

    auto cpptype = m_types.get_cpptype_of_qmltype(type.import, type.superclass);
    // Generate constructor (part 1)
    m_out << fmt::format(R"(
class {class_name} : public {superclass} {{
    Q_OBJECT

public:
    {class_name}() : {superclass}() {{
)", fmt::arg("class_name", Names::class_name(type.name)),
    fmt::arg("superclass", cpptype.name));

    m_out << "        // Properties" << endl;

    for (const auto &[_, property] : type.properties) {
        m_out << fmt::format(
R"(        {}.setBinding([&]() -> {} {{ return {}; }});
)",
            Names::value_name(property.name),
            m_types.get_cpptype_of_qmltype(property.type.import, property.type.name).name,
            JsCodegen(property.binding).generate_cpp());
    }

    m_out << endl;

    m_out << "        // Property bindings on the superclass" << endl;

    for (const auto &binding : type.bindings) {
        m_out << fmt::format(
R"(        auto {name}Binding = [&]() -> QVariant {{ return {binding}; }};
        setProperty("{name}", {name}Binding());
        {proxy_value_name}.setBinding({name}Binding);

        {{
            QQmlProperty property(this, "{name}");

            // Sync changes from the proxy property to the meta property
            if (property.isWritable()) {{
                connect(this, &{class_name}::{proxy_signal_name}, this, [this]() {{
                    setProperty("{name}", {proxy_value_name});
                }});
            }}

            // Sync changes from the meta property to the proxy property
            if (property.hasNotifySignal()) {{
                Q_ASSERT(property.connectNotifySignal(this, SLOT({proxy_setter_name}())));
            }}
        }}
)", fmt::arg("name", binding.name),
    fmt::arg("binding", JsCodegen(binding.binding).generate_cpp()),
    fmt::arg("class_name", Names::class_name(type.name)),
    fmt::arg("proxy_signal_name", Names::proxy_signal_name(binding.name)),
    fmt::arg("signal_name", Names::signal_name(binding.name)),
    fmt::arg("proxy_value_name", Names::proxy_value_name(binding.name)),
    fmt::arg("proxy_setter_name", Names::proxy_update_function_name(binding.name)));
    }

    m_out << endl;

    m_out << "        // Children" << endl;

    child_i = 0;
    for (const auto &child : type.children) {
        auto class_name = Names::child_class_name(type.name, child.import ? *child.import : "", child.superclass, child_i);
        m_out << fmt::format(
R"(        auto *child{i} = new {class_name}();
        static_cast<QObject *>(child{i})->setParent(this);
        if constexpr (std::is_base_of<QQuickItem, std::remove_pointer<decltype(this)>::type>::value && std::is_base_of<QQuickItem, {class_name}>::value) {{
            qobject_cast<QQuickItem *>(child{i})->setParentItem(qobject_cast<QQuickItem *>(this));
        }}

        // parent windows to the next window in the tree
        if constexpr (std::is_base_of<QQuickWindow, {class_name}>::value) {{
            if (auto childWindow = qobject_cast<QQuickWindow *>(child{i})) {{
                QMetaObject::invokeMethod(this, [this, childWindow]() {{ parentWindow(this, childWindow); }}, Qt::QueuedConnection);
            }}
        }}

)", fmt::arg("i", child_i),
    fmt::arg("class_name", class_name));
        child_i++;
    }

   // m_out << "    Q_ASSERT(" << "children().size()" << " == " << type.children.size() << ");" << endl;

    // Generate constructor (part 2)
    m_out << fmt::format(
R"(    }};
    ~{class_name}() override {{}};
)", fmt::arg("class_name", Names::class_name(type.name)));

    for (const auto &[_, property] : type.properties) {
        if (property.readonly) {
            m_out << fmt::format(
R"(
    Q_PROPERTY({prop_type} {prop_name} READ {getter_name} NOTIFY {signal_name} BINDABLE {bindable_name}))",
                fmt::arg("prop_type", m_types.get_cpptype_of_qmltype(property.type.import, property.type.name).name),
                fmt::arg("prop_name", property.name),
                fmt::arg("getter_name", Names::getter_name(property.name)),
                fmt::arg("setter_name", Names::setter_name(property.name)),
                fmt::arg("signal_name", Names::signal_name(property.name)),
                fmt::arg("bindable_name", Names::bindable_name(property.name)));
            m_out << std::endl;
        } else {
            m_out << fmt::format(
R"(
    Q_PROPERTY({prop_type} {prop_name} READ {getter_name} WRITE {setter_name} NOTIFY {signal_name} BINDABLE {bindable_name}))",
                fmt::arg("prop_type", m_types.get_cpptype_of_qmltype(property.type.import, property.type.name).name),
                fmt::arg("prop_name", property.name),
                fmt::arg("getter_name", Names::getter_name(property.name)),
                fmt::arg("setter_name", Names::setter_name(property.name)),
                fmt::arg("signal_name", Names::signal_name(property.name)),
                fmt::arg("bindable_name", Names::bindable_name(property.name)));
            m_out << std::endl;
        }

        m_out << fmt::format(R"(
public:
    inline {prop_type} {getter_name}() {{
        return {value_name};
    }};

    inline void {setter_name}(const {prop_type} &value) {{
        {value_name} = value;
        Q_EMIT {signal_name}();
    }};

    inline QBindable<{prop_type}> {bindable_name}() {{
        return QBindable<{prop_type}>(&{value_name});
    }}
)", fmt::arg("prop_name", property.name),
    fmt::arg("setter_name", Names::setter_name(property.name)),
    fmt::arg("getter_name", Names::getter_name(property.name)),
    fmt::arg("bindable_name", Names::bindable_name(property.name)),
    fmt::arg("prop_type", m_types.get_cpptype_of_qmltype(property.type.import, property.type.name).name),
    fmt::arg("value_name", Names::value_name(property.name)),
    fmt::arg("signal_name", Names::signal_name(property.name)),
    fmt::arg("class_name", Names::class_name(type.name)));

        m_out << fmt::format(R"(
    Q_SIGNAL void {signal_name}();

private:
    Q_OBJECT_BINDABLE_PROPERTY({class_name}, {prop_type}, {value_name}, &{class_name}::{signal_name})
)", fmt::arg("class_name", Names::class_name(type.name)),
fmt::arg("prop_type", m_types.get_cpptype_of_qmltype(property.type.import, property.type.name).name),
fmt::arg("value_name", Names::value_name(property.name)),
fmt::arg("signal_name", Names::signal_name(property.name)));
    }

    // Generate signals
    for (const auto &[_, method] : type.methods) {
        assert(method.type == IR::QmlMethod::Type::Signal); // Only signal is supported right now

        m_out << fmt::format(R"(
    Q_SIGNAL void {}()", method.name);

        if (!method.parameters.empty()) {
            std::for_each(method.parameters.begin(), method.parameters.end() - 1, [&](const auto &param) {
                m_out << fmt::format("{} {}, ", param.type_name, param.name);
            });
            const auto &last_method = method.parameters.back();
            m_out << fmt::format("{} {}", last_method.type_name, last_method.name);
        }

        m_out << ");" << std::endl;
    }

    for (const auto &binding : type.bindings) {
        m_out << fmt::format(R"(
private:
    Q_SIGNAL void {proxy_signal_name}();
    Q_OBJECT_BINDABLE_PROPERTY({class_name}, QVariant, {proxy_value_name}, &{class_name}::{proxy_signal_name});
    Q_SLOT void {proxy_setter_name}() {{
        {proxy_value_name} = property("{property_name}");
    }}
)", fmt::arg("class_name", Names::class_name(type.name)),
    fmt::arg("proxy_value_name", Names::proxy_value_name(binding.name)),
    fmt::arg("proxy_signal_name", Names::proxy_signal_name(binding.name)),
    fmt::arg("proxy_setter_name", Names::proxy_update_function_name(binding.name)),
    fmt::arg("property_name", binding.name));
    }

    m_out << "};" << std::endl;
}

void Codegen::generate_imports(const vector<IR::QmlType> &types)
{
   for (const auto &type : types) {
       auto cpptype = m_types.get_cpptype_of_qmltype(type.import, type.superclass);

       m_out << fmt::format(R"(#include <{include}>
)", fmt::arg("include", cpptype.header));

       generate_imports(type.children);

       for (const auto &[name, property] : type.properties) {
           auto cpptype = m_types.get_cpptype_of_qmltype(property.type.import, property.type.name);

            m_out << fmt::format(R"(#include <{include}>
)", fmt::arg("include", cpptype.header));
       }
   }
}

}
