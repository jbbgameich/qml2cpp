#pragma once

#include <jsast.h>
#include <sstream>

class JsCodegen
{
public:
    JsCodegen(const JsExpression &ast);

    std::string generate_cpp();

private:
    void begin_expression();
    void end_expression();
    void generate(shared_ptr<AstNode> ast);

    const JsExpression &expr;
    stringstream out;
};
