#include "jsast.h"

#include <iostream>
#include <cassert>
#include <ranges>

using namespace std;

namespace ranges = std::ranges;

JsExpression::JsExpression(string xmlast, optional<QmlTypeId> return_type)
    : ast(make_shared<ExpressionStatement>())
{
    // HACK, figure out if Qt outputs valid xml
    replace(xmlast, "\\\"", "'");

    //cout << xmlast << endl;

    pugi::xml_document doc;
    auto result = doc.load_string(xmlast.data());
    auto root = doc.root();

    cout << "==== Expression ===" << endl;
    load_ast(root, ast);

    const auto type = ast->infer_and_store_type(return_type);
    if (return_type) {
        cout << "-- Expected return type: " << *return_type << endl;
    }
    cout << "-- Inferred return type: " << type << endl;

    if (return_type && !Typesystem::instance().can_construct_from(*return_type, type)) {
        fatal_type_error(*return_type, type);
    }

    if (result.status != pugi::xml_parse_status::status_ok) {
        std::cout << result.description() << endl;
        UNREACHABLE;
    }
}

void JsExpression::load_ast(pugi::xml_node xml_parent, shared_ptr<AstNode> ast_parent, int depth)
{
    for (const auto &child : xml_parent.children()) {
        string_view name = child.name();
        shared_ptr<AstNode> ast_node = [&]() -> shared_ptr<AstNode> {
            if (name == "ExpressionStatement") {
                return make_shared<ExpressionStatement>();
            } else if (name == "StringLiteral") {
                auto node = make_shared<StringLiteral>();
                node->value = child.attribute("value").value();
                return node;
            } else if (name == "NumericLiteral") {
                auto node = make_shared<NumericLiteral>();
                node->value = child.attribute("value").value();
                return node;
            } else if (name == "BinaryExpression") {
                auto node = make_shared<BinaryExpression>();
                node->op = [&]{
                    string_view op = child.attribute("operatorToken").value();
                    if (op == "+")
                        return BinaryExpression::Add;
                    else if (op == "-")
                        return BinaryExpression::Sub;
                    else if (op == "*")
                        return BinaryExpression::Mul;
                    else if (op == "/")
                        return BinaryExpression::Div;
                    else if (op == "==")
                        return BinaryExpression::Eq;
                    else if (op == "!=")
                        return BinaryExpression::Neq;
                    else if (op == "&&")
                        return BinaryExpression::And;
                    else if (op == "||")
                        return BinaryExpression::Or;
                    UNREACHABLE;
                }();
                return node;
            } else if (name == "IdentifierExpression") {
                auto node = make_shared<IdentifierExpression>();
                node->name = child.attribute("name").value();
                return node;
            } else if (name == "ConditionalExpression") {
                return make_shared<ConditionalExpression>();
            } else if (name == "TrueLiteral") {
                return make_shared<TrueLiteral>();
            } else if (name == "FalseLiteral") {
                return make_shared<FalseLiteral>();
            } else if (name == "NotExpression") {
                return make_shared<NotExpression>();
            } else if (name == "NestedExpression") {
                return make_shared<NestedExpression>();
            }

            cerr << "Unhandled AST node " << name << endl;

            NOT_IMPLEMENTED;
        }();
        ast_parent->children.push_back(ast_node);

        indent(cout, depth);
        if (const auto literal_node = dynamic_pointer_cast<LiteralAstNode>(ast_node)) {
            cout << "<" << ast_node->node_name() << " value=" << literal_node->display_value() << ">" << endl;
        } else {
            cout << "<" << ast_node->node_name() << ">" << endl;
        }

        load_ast(child, ast_node, depth + 1);

        indent(cout, depth);
        cout << "</" << ast_node->node_name() << ">" << endl;
    }
}

QmlTypeId ConditionalExpression::infer_type(const optional<QmlTypeId> &hint) const {
    const auto conditionExpr = dynamic_pointer_cast<TypedAstNode>(condition());
    ASSERT_EXPLAIN(conditionExpr, "The expression in the condition of the ConditionalExpression needs to have a type");

    conditionExpr->infer_and_store_type(QmlTypeId("bool"));

    if (!Typesystem::instance().can_construct_from(QmlTypeId("bool"), conditionExpr->get_inferred_type())) {
        std::cerr << "The expression in a condition needs to be of type bool (or convertible to bool)" << endl;
        fatal_type_error(conditionExpr->infer_and_store_type(hint), QmlTypeId("bool"));
    }

    auto ifb = static_pointer_cast<TypedAstNode>(branch_if());
    auto elseb = static_pointer_cast<TypedAstNode>(branch_else());

    assert(ifb && elseb); // branch a and b need to be a typeable ast node
    ifb->infer_and_store_type(hint);
    elseb->infer_and_store_type(hint);

    if (ifb->get_inferred_type() == elseb->get_inferred_type()) {
        return ifb->get_inferred_type();
    } else {
        fatal_type_error(ifb->get_inferred_type(), elseb->get_inferred_type());
        UNREACHABLE;
    }
}

QmlTypeId ExpressionStatement::infer_type(const optional<QmlTypeId> &hint) const
{
    if (children.empty()) {
        UNREACHABLE // TMP
        if (hint) {
            return *hint;
        } else {
            UNREACHABLE;
            return QmlTypeId("void");
        }
    }

    if (auto typed_back = dynamic_pointer_cast<TypedAstNode>(children.back())) {
        return typed_back->infer_and_store_type(hint);
    } else {
        UNREACHABLE; // TMP
        if (hint) {
            return *hint;
        } else {
            return QmlTypeId("void");
        }
    }
}

void fatal_type_error(const QmlTypeId &expected, const QmlTypeId &actual) {
    bool types_match = expected == actual;
    ASSERT_EXPLAIN(!types_match, "Types matching can hardly be a type error xD");

    cerr << "Error: Types do not match: " << expected << " and " << actual << endl;
    exit(1);
}

QmlTypeId IdentifierExpression::infer_type(const optional<QmlTypeId> &hint) const {
    if (hint) { // fully rely on hint for now. TODO resolve identifier and typecheck
        return *hint;
    } else {
        return QmlTypeId("var");
    }
    NOT_IMPLEMENTED;
}

QmlTypeId BinaryExpression::infer_type(const optional<QmlTypeId> &hint) const {
    auto a = static_pointer_cast<TypedAstNode>(operand_a());
    auto b = static_pointer_cast<TypedAstNode>(operand_b());

    assert(a && b); // operand a and b need to be a typeable ast node

    switch (op) {
    case Add:
        a->infer_and_store_type(hint);
        b->infer_and_store_type(hint);

        if (!Typesystem::instance().can_add_to(b->get_inferred_type(), a->get_inferred_type())) {
            cerr << "Error: Cannot add " << b->get_inferred_type() << " to " << a->get_inferred_type() << endl;
            exit(1);
        }

        return Typesystem::instance().larger_num_type(a->get_inferred_type(), b->get_inferred_type());
    case Sub:
        a->infer_and_store_type(hint);
        b->infer_and_store_type(hint);

        if (!Typesystem::instance().can_substract_from(a->get_inferred_type(), b->get_inferred_type())) {
            cerr << "Error: Cannot substract " << b->get_inferred_type() << " from " << a->get_inferred_type() << endl;
            exit(1);
        }

        if (a->get_inferred_type() == b->get_inferred_type() && a->get_inferred_type() == "int")
            return QmlTypeId("int");
        else
            return QmlTypeId("double");
    case Mul:
        a->infer_and_store_type(hint);
        b->infer_and_store_type(hint);

        if (!Typesystem::instance().can_multiply(b->get_inferred_type(), a->get_inferred_type())) {
            cerr << "Error: Cannot multiply " << a->get_inferred_type() << " and " << b->get_inferred_type() << endl;
            exit(1);
        }

        return Typesystem::instance().larger_num_type(a->get_inferred_type(), b->get_inferred_type());
    case Div:
        a->infer_and_store_type(hint);
        b->infer_and_store_type(hint);

        if (!Typesystem::instance().can_divide(a->get_inferred_type(), b->get_inferred_type())) {
            cerr << "Error: Cannot divide " << a->get_inferred_type() << " and " << b->get_inferred_type() << endl;
            exit(1);
        }

        return QmlTypeId("double");
    case Eq:
    case Neq:
        // We cant pass on the type hint here, as == returns bool, and that says nothing about the type of the arguments
        a->infer_and_store_type(nullopt);
        b->infer_and_store_type(nullopt);

        if (!Typesystem::instance().can_compare_to(a->get_inferred_type(), b->get_inferred_type())) {
            cerr << "Error: Cannot compare " << a->get_inferred_type() << " and " << b->get_inferred_type() << endl;
            exit(1);
        }

        return QmlTypeId("bool");
    case And:
        a->infer_and_store_type(QmlTypeId("bool"));
        b->infer_and_store_type(QmlTypeId("bool"));
        if (!Typesystem::instance().can_construct_from(QmlTypeId("bool"), a->get_inferred_type())
                || !Typesystem::instance().can_construct_from(QmlTypeId("bool"), b->get_inferred_type())) {
            cerr << "Error: && can not be used with " << a->get_inferred_type() << " and " << b->get_inferred_type() << ".";
        }

        return QmlTypeId("bool");
    case Or:
        a->infer_and_store_type(QmlTypeId("bool"));
        b->infer_and_store_type(QmlTypeId("bool"));

        if (!Typesystem::instance().can_construct_from(QmlTypeId("bool"), a->get_inferred_type())
                || !Typesystem::instance().can_construct_from(QmlTypeId("bool"), b->get_inferred_type())) {
            cerr << "Error: || can not be used with " << a->get_inferred_type() << " and " << b->get_inferred_type() << ".";
        }

        return QmlTypeId("bool");
    }

    UNREACHABLE;
}

QmlTypeId NumericLiteral::infer_type(const optional<QmlTypeId> &hint) const {
    if (value.find('.') != string::npos) {
        if (hint && !Typesystem::instance().can_construct_from(*hint, QmlTypeId("double"))) {
            fatal_type_error(QmlTypeId("double"), *hint);
        }

        if (hint) {
            return Typesystem::instance().larger_num_type(QmlTypeId("real"), *hint);
        } else {
            // Else just assume the largest type, to be sure nothing gets lost.
            return QmlTypeId("double");
        }
    } else {
        if (hint && !Typesystem::instance().can_construct_from(*hint, QmlTypeId("int"))) {
            fatal_type_error(QmlTypeId("int"), *hint);
        }

        return QmlTypeId("int");
    }
}

QmlTypeId StringLiteral::infer_type(const optional<QmlTypeId> &hint) const {
    if (hint && !Typesystem::instance().can_construct_from(*hint, QmlTypeId("string"))) {
        fatal_type_error(QmlTypeId("string"), *hint);
    }

    return QmlTypeId("string");
}

QmlTypeId TypedAstNode::get_inferred_type() const
{
    ASSERT_EXPLAIN(inferred_type.has_value(), "Error: In a " << this->node_name() << ". Only call this after calling infer_and_store_type in the ast!");
    return *inferred_type;
}

QmlTypeId TypedAstNode::infer_and_store_type(const optional<QmlTypeId> &hint)
{
    // Infer type on this node
    inferred_type = infer_type(hint);

    ASSERT_EXPLAIN(inferred_type.has_value(), "We just set the inferred type to something, it can't be null");
    ASSERT_EXPLAIN(!inferred_type->name.empty(), "Returning an empty type is cheating");

    return *inferred_type;
}

QmlTypeId NestedExpression::infer_type(const optional<QmlTypeId> &hint) const
{
    return content()->infer_and_store_type(hint);
}

shared_ptr<TypedAstNode> NestedExpression::content() const
{
    ASSERT_EXPLAIN((children.size() == 1), "Nested expressions should have one child");

    auto typedChild = dynamic_pointer_cast<TypedAstNode>(children.back());
    ASSERT_EXPLAIN(typedChild, "Non-Typed nodes couldn't return something, and the point of this expression is to return something (I think).");

    return typedChild;
}
