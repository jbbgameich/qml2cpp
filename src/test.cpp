
#include <iostream>
#include <QDebug>

#include <main.qml.cpp>

#include <QGuiApplication>

int main(int argc, char** argv) {
    QGuiApplication app(argc, argv);
    Main m;
    std::cout << m.quadN() << std::endl << m.nPlusOne();
    qDebug() << m.r() << m.s();
    return app.exec();
}
