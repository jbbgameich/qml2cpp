#pragma once

#include <unordered_map>
#include <string>
#include <optional>
#include <vector>

using namespace std;

struct QmlTypeId {
    optional<string> import;
    string name;

    QmlTypeId() = default;

    QmlTypeId(optional<string> import, string name)
        : import(import)
        , name(name)
    {
    }

    QmlTypeId(string name)
        : import({})
        , name(name)
    {
    }

    bool operator==(const QmlTypeId &b) const {
        return import == b.import && name == b.name;
    };

    bool operator==(string_view name) const {
        return !import && name == name;
    };
};

inline ostream &operator<<(ostream &stream, const QmlTypeId &qmltype) {
    return stream << qmltype.name;
}

namespace std
{
    template<> struct hash<QmlTypeId>
    {
        std::size_t operator()(QmlTypeId const& s) const noexcept
        {
            std::size_t h1 = std::hash<std::optional<string>>{}(s.import);
            std::size_t h2 = std::hash<std::string>{}(s.name);
            return h1 ^ (h2 << 1);
        }
    };
}

struct CppTypeId {
    string header;
    string name;
};

namespace std
{
    template<> struct hash<CppTypeId>
    {
        std::size_t operator()(CppTypeId const& s) const noexcept
        {
            std::size_t h1 = std::hash<std::string>{}(s.header);
            std::size_t h2 = std::hash<std::string>{}(s.name);
            return h1 ^ (h2 << 1);
        }
    };
}

class Typesystem
{
public:
    static Typesystem &instance();

    CppTypeId get_cpptype_of_qmltype(const optional<string> &import, const string &qmlname) const;
    CppTypeId get_cpptype_of_qmltype(const QmlTypeId &type) const;
    void register_type(const string &import, const string &qmlname, const CppTypeId &cpptype);
    void register_builtin_type(const string &qmlname, const CppTypeId &cpptype);

    // Type traits
    bool can_construct_from(const QmlTypeId &type, const QmlTypeId &from) const;
    bool can_compare_to(const QmlTypeId &type, const QmlTypeId &to) const;
    bool can_add_to(const QmlTypeId &type, const QmlTypeId &to) const;
    bool can_substract_from(const QmlTypeId &type, const QmlTypeId &from) const;
    bool can_divide(const QmlTypeId &type, const QmlTypeId &by) const;
    bool can_multiply(const QmlTypeId &type, const QmlTypeId &to) const;

    QmlTypeId larger_num_type(const QmlTypeId &a, const QmlTypeId &b) const;

private:
    Typesystem();

    unordered_map<QmlTypeId, CppTypeId> m_qml_types;
    unordered_map<QmlTypeId, vector<QmlTypeId>> m_conversions;
    unordered_map<QmlTypeId, vector<QmlTypeId>> m_comparable;
    unordered_map<QmlTypeId, vector<QmlTypeId>> m_addable;
    unordered_map<QmlTypeId, vector<QmlTypeId>> m_substractable;
    unordered_map<QmlTypeId, vector<QmlTypeId>> m_dividable;
    unordered_map<QmlTypeId, vector<QmlTypeId>> m_multiplyable;
};

