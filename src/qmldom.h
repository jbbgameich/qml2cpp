#pragma once

#include <nlohmann/json.hpp>

#include <string_view>

using namespace std;

nlohmann::json qmldom(string_view qml_entrypoint);
