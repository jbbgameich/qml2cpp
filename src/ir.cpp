#include "ir.h"

#include <iostream>

#include "jsast.h"

using namespace std;

IR::IntermediateRepresentation::IntermediateRepresentation(const string &path, const nlohmann::json &dom)
    : qml_file(path)
{
    const auto qml_files = dom["qmlFileWithPath"];
    assert(qml_files.size() == 1); // we are only passing one file at the time to qmldom

    const auto file = qml_files.front();
    auto imports_json = file["currentItem"]["imports"];

    for (const auto &import_json : imports_json) {
        QmlImport import;
        import.importId = import_json.contains("importId") ? string(import_json["importId"]) : optional<string>(nullopt);
        import.uri = import_json["uri"];

        imports[import.importId ? import.importId.value() : import.uri] = move(import);
    }

    auto components = file["currentItem"]["components"];
    for (const auto &ns : components) {
        for (const auto &component : ns) {
            auto type = parse_qml_component(component);
            qml_types[type.name] = std::move(type);
        }
    }
}

QmlTypeId IR::IntermediateRepresentation::split_namespace_and_type(std::string_view name) const {
    auto doti = name.find('.');

    if (doti != std::string::npos) {
        auto qmlnamespace = string(name.begin(), name.begin() + doti);
        auto import = imports.at(qmlnamespace).uri;
        auto type = string(name.begin() + doti + 1, name.end());

        return {import, type};
    } else {
        return {{}, string(name)}; // FIXME in normal QML code not everything without namespace is a builtin type
    }
}

IR::QmlType IR::IntermediateRepresentation::parse_qml_type(const nlohmann::json &obj)
{
    QmlType type;

    if (obj.contains("idStr")) {
        type.id = obj["idStr"];
    }

    auto import_and_superclass = string(obj["name"]);
    const auto [import, superclass] = split_namespace_and_type(import_and_superclass);
    type.import = import;
    type.superclass = superclass;

    for (const auto &propertyDef : obj["propertyDefs"]) {
        auto propDef = propertyDef[0]; // TODO why
        const auto prop_type = split_namespace_and_type(string(propDef["typeName"]));
        QmlProperty property {
            prop_type,
            propDef["name"],
            JsExpression(prop_type),
            bool(propDef["isReadonly"])
        };
        type.properties[property.name] = std::move(property);
    }

    for (const auto &binding : obj["bindings"]) {
        auto propBind = binding[0]; // TODO why

        // Property is declared in this type
        if (type.properties.contains(propBind["name"])) {
            auto &property = type.properties[propBind["name"]];

            string jsast = propBind["value"]["astRelocatableDump"];
            property.binding = JsExpression(jsast, property.type);
        } else { // Property is set on the superclass
            QmlBinding binding {
                propBind["name"],
                JsExpression(string(propBind["value"]["astRelocatableDump"]), {}),
            };
            type.bindings.push_back(move(binding));
        }
    }

    for (const auto &method : obj["methods"]) {
        auto firstMethod = method[0]; // TODO overloads?
        QmlMethod m;
        m.name = firstMethod["name"];
        m.type = firstMethod["methodType"];

        for (const auto &parameter : firstMethod["parameters"]) {
            QmlParameter param;
            param.name = parameter["name"];
            param.type_name = parameter["typeName"];
            m.parameters.push_back(std::move(param));
        }

        type.methods[m.name] = std::move(m);
    }

    for (const auto &child : obj["children"]) {
        QmlType childType = parse_qml_type(child);
        type.children.push_back(move(childType));
    }

    return type;
}

IR::QmlType IR::IntermediateRepresentation::parse_qml_component(const nlohmann::json &component)
{
    QmlType type;

    bool i = false;
    for (const auto &obj : component["objects"]) {
        assert(!i);
        type = parse_qml_type(obj);
        i = true;
    }

    type.name = component["name"];

    return type;
}
