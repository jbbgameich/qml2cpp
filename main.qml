import QtQml 2.15 as QML
import QtQuick 2.7 as QtQuick
import QtQuick.Window 2.15 as Window

Window.Window {
    id: window
    readonly property int n: 3 + 3

    property int quadN: n + 4 + 2 + 3 + 2 + 4 * 3 + 6 + 7 + 8 - 9 / 1

    property int nPlusOne: 0 + 1

    property date d: "2020-12-31 23:59"

    property point p: "1,2"

    property rect r: "51,51,123x123"

    property size s: "50x50"

    property url debian: "https://debian.org"

    signal boom(int time)

    visible: s == s && true
    title: debian == debian && true ? "Hello World" : ":("

    property var title2: debian
    width: 500 + 1.5
    height: 400

    property real re: 3.1415

    QtQuick.Item {
    }
    QtQuick.Item {
    }
    QtQuick.Item {
        QtQuick.Item {
            property int lol: 0

            Window.Window {
                visible: true
                title: "Window 2"
                width: 250
                height: 200
                x: 500
                y: 0

                QtQuick.Item {
                    QML.QtObject {
                        QtQuick.Item {
                            QtQuick.Item {
                                QtQuick.Item {
                                    QtQuick.Item {
                                        QtQuick.Item {
                                            QtQuick.Item {
                                                QtQuick.Item {
                                                    QtQuick.Item {
                                                        QtQuick.Item {
                                                            QtQuick.Item {
                                                                QtQuick.Item {
                                                                    QtQuick.Item {
                                                                        QtQuick.Item {
                                                                            QtQuick.Item {
                                                                                QtQuick.Item {
                                                                                    QtQuick.Item {
                                                                                        QtQuick.Item {
                                                                                            QtQuick.Item {
                                                                                                QtQuick.Item {
                                                                                                    QtQuick.Item {
                                                                                                        QtQuick.Item {
                                                                                                            QtQuick.Item {
                                                                                                                QtQuick.Item {
                                                                                                                    QtQuick.Item {
                                                                                                                        QtQuick.Item {
                                                                                                                            QtQuick.Item {
                                                                                                                                QtQuick.Item {
                                                                                                                                    QtQuick.Item {
                                                                                                                                        QtQuick.Item {
                                                                                                                                            QtQuick.Item {
                                                                                                                                                QtQuick.Item {
                                                                                                                                                    QtQuick.Item {
                                                                                                                                                        QtQuick.Item {
                                                                                                                                                            QtQuick.Item {
                                                                                                                                                                QtQuick.Item {
                                                                                                                                                                    QtQuick.Item {
                                                                                                                                                                        QtQuick.Item {
                                                                                                                                                                            QtQuick.Item {
                                                                                                                                                                                QtQuick.Item {
                                                                                                                                                                                    QtQuick.Item {
                                                                                                                                                                                        QtQuick.Item {
                                                                                                                                                                                            x: 5
                                                                                                                                                                                            y: 20

                                                                                                                                                                                            QtQuick.Item {
                                                                                                                                                                                                QtQuick.Item {
                                                                                                                                                                                                    QtQuick.Item {
                                                                                                                                                                                                        QtQuick.Item {
                                                                                                                                                                                                            QtQuick.Item {
                                                                                                                                                                                                                QtQuick.Item {
                                                                                                                                                                                                                    QtQuick.Item {
                                                                                                                                                                                                                        QtQuick.Item {
                                                                                                                                                                                                                            QtQuick.Item {
                                                                                                                                                                                                                                QtQuick.Item {
                                                                                                                                                                                                                                    QtQuick.Item {
                                                                                                                                                                                                                                        QtQuick.Item {
                                                                                                                                                                                                                                            QtQuick.Item {
                                                                                                                                                                                                                                                QtQuick.Item {
                                                                                                                                                                                                                                                    QtQuick.Item {
                                                                                                                                                                                                                                                        QtQuick.Item {
                                                                                                                                                                                                                                                            QtQuick.Item {
                                                                                                                                                                                                                                                                QtQuick.Item {
                                                                                                                                                                                                                                                                    QtQuick.Item {
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    Add {
        a: 4
        b: 8
    }
    Sub {
        a: 8
        b: 4
    }
}
