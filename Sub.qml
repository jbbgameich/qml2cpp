import QtQml as QML

QML.QtObject {
	property int a
	property int b
	property int result: a - b
}
